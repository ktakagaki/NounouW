(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     19870,        692]
NotebookOptionsPosition[     15689,        544]
NotebookOutlinePosition[     17217,        589]
CellTagsIndexPosition[     17132,        584]
WindowTitle->NNDetectorPlot - Wolfram Mathematica
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[" ", "SymbolColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 0}}],

Cell[TextData[{
 ButtonBox["Mathematica",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:NounouW/guide/NounouW"],
 StyleBox[" > ", "LinkTrailSeparator"]
}], "LinkTrail"],

Cell[BoxData[GridBox[{
   {Cell["NOUNOUW PACLET SYMBOL", "PacletNameCell"], Cell[TextData[Cell[
    BoxData[
     ActionMenuBox[
      FrameBox["\<\"URL \[RightGuillemet]\"\>",
       StripOnInput->False], {"\<\"NounouW/ref/NNDetectorPlot\"\>":>
      None, "\<\"Copy Mathematica url\"\>":>
      Module[{DocumentationSearch`Private`nb$}, 
       DocumentationSearch`Private`nb$ = NotebookPut[
          Notebook[{
            Cell["NounouW/ref/NNDetectorPlot"]}, Visible -> False]]; 
       SelectionMove[DocumentationSearch`Private`nb$, All, Notebook]; 
       FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
       NotebookClose[DocumentationSearch`Private`nb$]; Null], 
      Delimiter, "\<\"Copy web url\"\>":>
      Module[{DocumentationSearch`Private`nb$}, 
       DocumentationSearch`Private`nb$ = NotebookPut[
          Notebook[{
            Cell[
             BoxData[
              MakeBoxes[
               Hyperlink[
               "http://reference.wolfram.com/mathematica/NounouW/ref/\
NNDetectorPlot.html"], StandardForm]], "Input", TextClipboardType -> 
             "PlainText"]}, Visible -> False]]; 
       SelectionMove[DocumentationSearch`Private`nb$, All, Notebook]; 
       FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
       NotebookClose[DocumentationSearch`Private`nb$]; 
       Null], "\<\"Go to web url\"\>":>FrontEndExecute[{
        NotebookLocate[{
          URL[
           StringJoin[
           "http://reference.wolfram.com/mathematica/", 
            "NounouW/ref/NNDetectorPlot", ".html"]], None}]}]},
      Appearance->None,
      MenuAppearance->Automatic]],
     LineSpacing->{1.4, 0}]], "AnchorBar"]}
  }]], "AnchorBarGrid",
 GridBoxOptions->{GridBoxItemSize->{"Columns" -> {
     Scaled[0.65], {
      Scaled[0.34]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, 
   "RowsIndexed" -> {}}},
 CellID->1],

Cell["NNDetectorPlot", "ObjectName",
 CellID->1224892054],

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{"NNDetectorPlot", "[", "]"}]], "InlineFormula"],
     " \[LineSeparator]NNDetectorPlot"
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, "Rows" -> {{None}}, 
   "RowsIndexed" -> {}}},
 CellID->982511436],

Cell[CellGroupData[{

Cell[TextData[ButtonBox["EXAMPLES",
 BaseStyle->None,
 Appearance->{Automatic, None},
 Evaluator->None,
 Method->"Preemptive",
 ButtonFunction:>(FrontEndExecute[{
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], All, ButtonCell], 
    FrontEndToken["OpenCloseGroup"], 
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], After, 
     CellContents]}]& )]], "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->73306118],

Cell[CellGroupData[{

Cell[TextData[{
 "Basic Examples",
 "\[NonBreakingSpace]\[NonBreakingSpace]",
 Cell["(1)", "ExampleCount"]
}], "ExampleSection",
 CellID->311268041],

Cell[BoxData[
 RowBox[{"<<", "NounouW`"}]], "Input",
 CellLabel->"In[1]:=",
 CellID->182887016],

Cell[BoxData[
 RowBox[{
  RowBox[{"layoutArr", " ", "=", " ", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"-", "1"}], ",", " ", "15", ",", "14", ",", "13", ",", "12", 
       ",", "11"}], "}"}], ",", " ", 
     RowBox[{"{", 
      RowBox[{
      "10", ",", " ", "9", ",", " ", "8", ",", " ", "1", ",", " ", "0"}], 
      "}"}], ",", " ", 
     RowBox[{"{", 
      RowBox[{"7", ",", " ", "6", ",", " ", "5", ",", " ", "4"}], "}"}], ",", 
     " ", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"-", "1"}], ",", " ", "3", ",", "2"}], "}"}]}], "}"}]}], 
  ";"}]], "Input",
 CellLabel->"In[2]:=",
 CellID->121274972],

Cell[BoxData[
 RowBox[{"AllowRaggedArrays", "[", "True", "]"}]], "Input",
 CellLabel->"In[3]:=",
 CellID->514242286],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"layout", " ", "=", 
  RowBox[{"JavaNew", "[", 
   RowBox[{
   "\"\<nounou.elements.layouts.NNDataLayoutHexagonal\>\"", ",", " ", 
    "layoutArr"}], "]"}]}]], "Input",
 CellLabel->"In[4]:=",
 CellID->687037852],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\[LeftGuillemet]", 
   RowBox[{"JavaObject", "[", 
    RowBox[{
    "nounou", ".", "elements", ".", "layouts", ".", "NNDataLayoutHexagonal"}],
     "]"}], "\[RightGuillemet]"}],
  JLink`Objects`vm2`JavaObject23456350864408577]], "Output",
 ImageSize->{543, 17},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[4]=",
 CellID->504254057]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"layout", "@", 
  RowBox[{"getChannelCount", "[", "]"}]}]], "Input",
 CellLabel->"In[5]:=",
 CellID->176169232],

Cell[BoxData["16"], "Output",
 ImageSize->{22, 17},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[5]=",
 CellID->220948255]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"layout", "@", 
  RowBox[{"getChannelRadius", "[", "]"}]}]], "Input",
 CellLabel->"In[6]:=",
 CellID->12581516],

Cell[BoxData["50.`"], "Output",
 ImageSize->{31, 17},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[6]=",
 CellID->194523598]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"layout", "@", 
  RowBox[{"layoutArray", "[", "]"}]}]], "Input",
 CellLabel->"In[7]:=",
 CellID->186302334],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"-", "1"}], ",", "15", ",", "14", ",", "13", ",", "12", ",", 
     "11"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"10", ",", "9", ",", "8", ",", "1", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"7", ",", "6", ",", "5", ",", "4"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"-", "1"}], ",", "3", ",", "2"}], "}"}]}], "}"}]], "Output",
 ImageSize->{568, 17},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[7]=",
 CellID->714609180]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"layout", "@", 
  RowBox[{"toJsonString", "[", "]"}]}]], "Input",
 CellLabel->"In[8]:=",
 CellID->32444361],

Cell[BoxData["\<\"{\\\"layoutArray\\\":[[-1,15,14,13,12,11],[10,9,8,1,0],[7,6,\
5,4],[-1,3,2]],\\\"channelRadius\\\":50.0,\\\"channelDistance\\\":100.0,\\\"\
masked\\\":[],\\\"className\\\":\\\"nounou.elements.layouts.\
NNDataLayoutHexagonal\\\",\\\"gitHead\\\":\\\"\
674199eda7404048d811c228fb848bd74709fd6a\\\",\\\"version\\\":0.5,\\\"bitmap$0\
\\\":false}\"\>"], "Output",
 ImageSize->{582, 94},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[8]=",
 CellID->96204095]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"NNDetectorPlot", "[", "layout", "]"}]], "Input",
 CellLabel->"In[9]:=",
 CellID->200294427],

Cell[BoxData[
 RowBox[{
  StyleBox[
   RowBox[{"NNToList", "::", "invalidArgs"}], "MessageName"], ":", 
  " ", "\<\"Function called with invalid arguments \
\[NoBreak]\\!\\({\\*InterpretationBox[\\(\[LeftGuillemet] \
\\(\\(JavaObject[nounou.elements.layouts.NNDataLayoutHexagonal]\\)\\) \
\[RightGuillemet]\\), JLink`Objects`vm2`JavaObject23456350864408577]}\\)\
\[NoBreak].\"\>"}]], "Message", "MSG",
 CellLabel->"During evaluation of In[9]:=",
 CellID->203163290]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"InstanceOf", "[", 
  RowBox[{"layout", ",", " ", "$NNDataLayoutSpatialClass"}], "]"}]], "Input",
 CellLabel->"In[10]:=",
 CellID->410026117],

Cell[BoxData["True"], "Output",
 ImageSize->{40, 17},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[10]=",
 CellID->400834876]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"JavaObjectQ", "[", 
  RowBox[{"layou", " ", "qt"}], "]"}]], "Input",
 CellLabel->"In[11]:=",
 CellID->590624524],

Cell[BoxData["True"], "Output",
 ImageSize->{40, 17},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[11]=",
 CellID->105287466]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"HHJavaObjectQ", "[", 
  RowBox[{"layout", ",", "$NNDataLayoutSpatialClass"}], "]"}]], "Input",
 CellLabel->"In[12]:=",
 CellID->469911287],

Cell[BoxData["False"], "Output",
 ImageSize->{49, 17},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[12]=",
 CellID->25647138]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"HHJavaObjectQ", "[", 
  RowBox[{
  "layout", ",", "\"\<nounou.elements.layouts.NNDataLayoutHexagonal\>\""}], 
  "]"}]], "Input",
 CellLabel->"In[13]:=",
 CellID->72937],

Cell[BoxData["False"], "Output",
 ImageSize->{49, 17},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[13]=",
 CellID->58582956]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"HHJavaObjectQ", "[", "layout", "]"}]], "Input",
 CellLabel->"In[14]:=",
 CellID->214773236],

Cell[BoxData["False"], "Output",
 ImageSize->{49, 17},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[14]=",
 CellID->145106188]
}, Open  ]],

Cell[BoxData[
 RowBox[{"Definition", "[", "HHJavaObjectQ", "]"}]], "Input",
 CellLabel->"In[15]:=",
 CellID->171120571],

Cell[CellGroupData[{

Cell[BoxData["\[IndentingNewLine]"], "Input",
 CellLabel->"In[16]:=",
 CellID->88241639],

Cell[BoxData[
 InterpretationBox[GridBox[{
    {GridBox[{
       {
        RowBox[{
         RowBox[{"HHJavaObjectQ", "[", 
          RowBox[{"HokahokaW`Java`Private`x_", "/;", 
           RowBox[{
           "HokahokaW`Java`Private`JavaObjectQ", "[", 
            "HokahokaW`Java`Private`x", "]"}]}], "]"}], ":=", "True"}]},
       {" "},
       {
        RowBox[{
         RowBox[{"HHJavaObjectQ", "[", 
          RowBox[{
           RowBox[{"HokahokaW`Java`Private`x_", "/;", 
            RowBox[{
            "HokahokaW`Java`Private`JavaObjectQ", "[", 
             "HokahokaW`Java`Private`x", "]"}]}], ",", 
           "HokahokaW`Java`Private`className_String"}], "]"}], ":=", 
         RowBox[{"Module", "[", 
          RowBox[{
           RowBox[{"{", "}"}], ",", 
           RowBox[{
            RowBox[{"Print", "[", "\<\"Hello\"\>", "]"}], ";", 
            RowBox[{"HokahokaW`Java`Private`InstanceOf", "[", 
             RowBox[{
             "HokahokaW`Java`Private`x", ",", 
              "HokahokaW`Java`Private`className"}], "]"}]}]}], "]"}]}]},
       {" "},
       {
        RowBox[{
         RowBox[{"HHJavaObjectQ", "[", "___", "]"}], ":=", "False"}]}
      },
      BaselinePosition->{Baseline, {1, 1}},
      GridBoxAlignment->{
       "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
        "RowsIndexed" -> {}},
      GridBoxItemSize->{"Columns" -> {{
           Scaled[0.999]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, 
        "RowsIndexed" -> {}}]}
   },
   BaselinePosition->{Baseline, {1, 1}},
   GridBoxAlignment->{
    "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
     "RowsIndexed" -> {}}],
  Definition[HokahokaW`Java`HHJavaObjectQ],
  Editable->False]], "Output",
 ImageSize->{583, 207},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[16]=",
 CellID->355031587]
}, Open  ]],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"HHJavaObjectQ", "[", 
     RowBox[{"x_", "/;", 
      RowBox[{"JavaObjectQ", "[", "x", "]"}]}], "]"}], ":=", "True"}], ";"}], 
  "\[IndentingNewLine]"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"HHJavaObjectQ", "[", 
     RowBox[{
      RowBox[{"x_", "/;", 
       RowBox[{"JavaObjectQ", "[", "x", "]"}]}], ",", "className_String"}], 
     "]"}], ":=", 
    RowBox[{"Module", "[", 
     RowBox[{
      RowBox[{"{", "}"}], ",", 
      RowBox[{
       RowBox[{"Print", "[", "\"\<Hello\>\"", "]"}], ";", 
       "\[IndentingNewLine]", 
       RowBox[{"InstanceOf", "[", 
        RowBox[{"x", ",", "className"}], "]"}]}]}], "]"}]}], ";"}], 
  "\[IndentingNewLine]"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"HHJavaObjectQ", "[", "___", "]"}], ":=", "False"}], 
  ";"}]}], "Input",
 CellLabel->"In[17]:=",
 CellID->2360046],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Definition", "[", "HHJavaObjectQ", "]"}]], "Input",
 CellLabel->"In[18]:=",
 CellID->47136202],

Cell[BoxData[
 InterpretationBox[GridBox[{
    {GridBox[{
       {
        RowBox[{
         RowBox[{"HHJavaObjectQ", "[", 
          RowBox[{"HokahokaW`Java`Private`x_", "/;", 
           RowBox[{
           "HokahokaW`Java`Private`JavaObjectQ", "[", 
            "HokahokaW`Java`Private`x", "]"}]}], "]"}], ":=", "True"}]},
       {" "},
       {
        RowBox[{
         RowBox[{"HHJavaObjectQ", "[", 
          RowBox[{
           RowBox[{"HokahokaW`Java`Private`x_", "/;", 
            RowBox[{
            "HokahokaW`Java`Private`JavaObjectQ", "[", 
             "HokahokaW`Java`Private`x", "]"}]}], ",", 
           "HokahokaW`Java`Private`className_String"}], "]"}], ":=", 
         RowBox[{"Module", "[", 
          RowBox[{
           RowBox[{"{", "}"}], ",", 
           RowBox[{
            RowBox[{"Print", "[", "\<\"Hello\"\>", "]"}], ";", 
            RowBox[{"HokahokaW`Java`Private`InstanceOf", "[", 
             RowBox[{
             "HokahokaW`Java`Private`x", ",", 
              "HokahokaW`Java`Private`className"}], "]"}]}]}], "]"}]}]},
       {" "},
       {
        RowBox[{
         RowBox[{"HHJavaObjectQ", "[", 
          RowBox[{"x_", "/;", 
           RowBox[{"JavaObjectQ", "[", "x", "]"}]}], "]"}], ":=", "True"}]},
       {" "},
       {
        RowBox[{
         RowBox[{"HHJavaObjectQ", "[", 
          RowBox[{
           RowBox[{"x_", "/;", 
            RowBox[{"JavaObjectQ", "[", "x", "]"}]}], ",", 
           "className_String"}], "]"}], ":=", 
         RowBox[{"Module", "[", 
          RowBox[{
           RowBox[{"{", "}"}], ",", 
           RowBox[{
            RowBox[{"Print", "[", "\<\"Hello\"\>", "]"}], ";", 
            RowBox[{"InstanceOf", "[", 
             RowBox[{"x", ",", "className"}], "]"}]}]}], "]"}]}]},
       {" "},
       {
        RowBox[{
         RowBox[{"HHJavaObjectQ", "[", "___", "]"}], ":=", "False"}]}
      },
      BaselinePosition->{Baseline, {1, 1}},
      GridBoxAlignment->{
       "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
        "RowsIndexed" -> {}},
      GridBoxItemSize->{"Columns" -> {{
           Scaled[0.999]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, 
        "RowsIndexed" -> {}}]}
   },
   BaselinePosition->{Baseline, {1, 1}},
   GridBoxAlignment->{
    "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
     "RowsIndexed" -> {}}],
  Definition[HokahokaW`Java`HHJavaObjectQ],
  Editable->False]], "Output",
 ImageSize->{583, 301},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[18]=",
 CellID->627200296]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"NNDetectorPlot - Wolfram Mathematica",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "built" -> "{2016, 8, 4, 11, 16, 44.7796439}", "context" -> "NounouW`", 
    "keywords" -> {}, "index" -> True, "label" -> "NounouW Paclet Symbol", 
    "language" -> "en", "paclet" -> "NounouW", "status" -> "None", "summary" -> 
    "NNDetectorPlot[] NNDetectorPlot", "synonyms" -> {}, "title" -> 
    "NNDetectorPlot", "type" -> "Symbol", "uri" -> 
    "NounouW/ref/NNDetectorPlot"}, "LinkTrails" -> "", "SearchTextTranslated" -> 
  ""},
CellContext->"Global`",
FrontEndVersion->"9.0 for Microsoft Windows (64-bit) (January 25, 2013)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[
    StyleDefinitions -> FrontEnd`FileName[{"Wolfram"}, "Reference.nb"]]], 
   Cell[
    StyleData["Input"], CellContext -> "Global`"], 
   Cell[
    StyleData["Output"], CellContext -> "Global`"]}, Visible -> False, 
  FrontEndVersion -> "9.0 for Microsoft Windows (64-bit) (January 25, 2013)", 
  StyleDefinitions -> "Default.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "PrimaryExamplesSection"->{
  Cell[3183, 93, 460, 13, 70, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->73306118]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"PrimaryExamplesSection", 16991, 577}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[607, 21, 76, 1, 70, "SymbolColorBar"],
Cell[686, 24, 180, 5, 70, "LinkTrail"],
Cell[869, 31, 1877, 42, 70, "AnchorBarGrid",
 CellID->1],
Cell[2749, 75, 57, 1, 70, "ObjectName",
 CellID->1224892054],
Cell[2809, 78, 349, 11, 70, "Usage",
 CellID->982511436],
Cell[CellGroupData[{
Cell[3183, 93, 460, 13, 70, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->73306118],
Cell[CellGroupData[{
Cell[3668, 110, 148, 5, 70, "ExampleSection",
 CellID->311268041],
Cell[3819, 117, 95, 3, 70, "Input",
 CellID->182887016],
Cell[3917, 122, 652, 21, 70, "Input",
 CellID->121274972],
Cell[4572, 145, 116, 3, 70, "Input",
 CellID->514242286],
Cell[CellGroupData[{
Cell[4713, 152, 234, 7, 70, "Input",
 CellID->687037852],
Cell[4950, 161, 414, 12, 38, "Output",
 CellID->504254057]
}, Open  ]],
Cell[CellGroupData[{
Cell[5401, 178, 134, 4, 70, "Input",
 CellID->176169232],
Cell[5538, 184, 159, 5, 38, "Output",
 CellID->220948255]
}, Open  ]],
Cell[CellGroupData[{
Cell[5734, 194, 134, 4, 70, "Input",
 CellID->12581516],
Cell[5871, 200, 161, 5, 38, "Output",
 CellID->194523598]
}, Open  ]],
Cell[CellGroupData[{
Cell[6069, 210, 130, 4, 70, "Input",
 CellID->186302334],
Cell[6202, 216, 574, 18, 38, "Output",
 CellID->714609180]
}, Open  ]],
Cell[CellGroupData[{
Cell[6813, 239, 130, 4, 70, "Input",
 CellID->32444361],
Cell[6946, 245, 505, 10, 115, "Output",
 CellID->96204095]
}, Open  ]],
Cell[CellGroupData[{
Cell[7488, 260, 115, 3, 70, "Input",
 CellID->200294427],
Cell[7606, 265, 465, 10, 70, "Message",
 CellID->203163290]
}, Open  ]],
Cell[CellGroupData[{
Cell[8108, 280, 164, 4, 70, "Input",
 CellID->410026117],
Cell[8275, 286, 162, 5, 38, "Output",
 CellID->400834876]
}, Open  ]],
Cell[CellGroupData[{
Cell[8474, 296, 136, 4, 70, "Input",
 CellID->590624524],
Cell[8613, 302, 162, 5, 38, "Output",
 CellID->105287466]
}, Open  ]],
Cell[CellGroupData[{
Cell[8812, 312, 162, 4, 70, "Input",
 CellID->469911287],
Cell[8977, 318, 162, 5, 38, "Output",
 CellID->25647138]
}, Open  ]],
Cell[CellGroupData[{
Cell[9176, 328, 192, 6, 70, "Input",
 CellID->72937],
Cell[9371, 336, 162, 5, 38, "Output",
 CellID->58582956]
}, Open  ]],
Cell[CellGroupData[{
Cell[9570, 346, 115, 3, 70, "Input",
 CellID->214773236],
Cell[9688, 351, 163, 5, 38, "Output",
 CellID->145106188]
}, Open  ]],
Cell[9866, 359, 119, 3, 70, "Input",
 CellID->171120571],
Cell[CellGroupData[{
Cell[10010, 366, 88, 2, 70, "Input",
 CellID->88241639],
Cell[10101, 370, 1882, 52, 228, "Output",
 CellID->355031587]
}, Open  ]],
Cell[11998, 425, 891, 30, 70, "Input",
 CellID->2360046],
Cell[CellGroupData[{
Cell[12914, 459, 118, 3, 70, "Input",
 CellID->47136202],
Cell[13035, 464, 2588, 73, 322, "Output",
 CellID->627200296]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[15662, 542, 23, 0, 70, "FooterCell"]
}
]
*)

(* End of internal cache information *)

