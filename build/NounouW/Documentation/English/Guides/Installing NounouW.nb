(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[      2518,         85]
NotebookOptionsPosition[      1326,         50]
NotebookOutlinePosition[      2376,         79]
CellTagsIndexPosition[      2333,         76]
WindowTitle->Installing NounouW - Wolfram Mathematica
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[" ", "GuideColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 0}}],

Cell[TextData[{
 ButtonBox["Mathematica",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:NounouW/guide/NounouW"],
 StyleBox[" > ", "LinkTrailSeparator"]
}], "LinkTrail"],

Cell[CellGroupData[{

Cell["Installing NounouW", "GuideTitle",
 CellID->942062912],

Cell[TextData[{
 "See the ",
 ButtonBox["GitHub page",
  BaseStyle->"Hyperlink",
  ButtonData->{
    URL["https://github.com/ktakagaki/NounouW/wiki/Installing-NounouW"], None},
  ButtonNote->"https://github.com/ktakagaki/NounouW/wiki/Installing-NounouW"],
 " on how to install NounouW"
}], "GuideAbstract",
 CellID->2001916300]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"Installing NounouW - Wolfram Mathematica",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "built" -> "{2016, 8, 4, 11, 16, 43.5415731}", "context" -> "NounouW`", 
    "keywords" -> {}, "index" -> True, "label" -> "Nounou W Guide", 
    "language" -> "en", "paclet" -> "NounouW", "status" -> "None", "summary" -> 
    "See the GitHub page on how to install NounouW", "synonyms" -> {}, 
    "title" -> "Installing NounouW", "type" -> "Guide", "uri" -> 
    "NounouW/guide/Installing NounouW"}, "LinkTrails" -> "", 
  "SearchTextTranslated" -> ""},
FrontEndVersion->"9.0 for Microsoft Windows (64-bit) (January 25, 2013)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "Reference.nb", 
  CharacterEncoding -> "ShiftJIS"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[611, 21, 75, 1, 70, "GuideColorBar"],
Cell[689, 24, 180, 5, 70, "LinkTrail"],
Cell[CellGroupData[{
Cell[894, 33, 60, 1, 70, "GuideTitle",
 CellID->942062912],
Cell[957, 36, 327, 9, 70, "GuideAbstract",
 CellID->2001916300]
}, Open  ]],
Cell[1299, 48, 23, 0, 70, "FooterCell"]
}
]
*)

(* End of internal cache information *)

